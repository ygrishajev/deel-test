module.exports = {
  verbose: true,
  testEnvironment: 'node',
  setupFiles: ['./spec/setup.js']
};
