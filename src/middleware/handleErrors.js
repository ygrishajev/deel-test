import { isHttpError } from 'http-errors';
import { ValidationError } from 'express-json-validator-middleware';

// eslint-disable-next-line no-unused-vars
export const handleErrors = (error, req, res, next) => {
  if (isHttpError(error)) {
    res.status(error.status).json({
      message: error.message,
      status: error.status,
      error: error.constructor.name
    });
  } else if (error instanceof ValidationError) {
    res.status(400).send(error.validationErrors);
  } else {
    res.status(500).json({
      message: 'InternalServerError',
      status: 500,
      error: 'InternalServerError'
    });
  }
};

export const handlePipeline = (...middlewares) => middlewares.map(handleMiddleware);

function handleMiddleware(middleware) {
  return async (...args) => {
    try {
      return await middleware(...args);
    } catch (error) {
      const { 2: next } = args;
      return next(error);
    }
  };
}
