import assert from 'http-assert';

export const getCurrentUser = async (req, res, next) => {
  const profileId = req.get('profile_id');
  assert(profileId, 401);

  const { Profile } = req.app.get('models');
  const profile = await Profile.findByPk(profileId);

  assert(profile, 401);

  req.currentUser = profile;
  next();
};
