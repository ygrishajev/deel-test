import express from 'express';
import bodyParser from 'body-parser';

import { sequelize } from './models';
import { getCurrentUser, handleErrors, handlePipeline } from './middleware';
import { contract, job, profile, admin } from './controllers';

const app = express();

app.use(bodyParser.json());
app.set('sequelize', sequelize);
app.set('models', sequelize.models);

app.get('/contracts/', ...handlePipeline(getCurrentUser, ...contract.pipelines.findAll));
app.get('/contracts/:id', ...handlePipeline(getCurrentUser, contract.findOne));

app.get('/jobs', ...handlePipeline(getCurrentUser, ...job.pipelines.findAll));
app.post('/jobs/:jobId/pay', ...handlePipeline(getCurrentUser, job.pay));

app.post('/balances/deposit/:userId', ...handlePipeline(getCurrentUser, ...profile.pipelines.deposit));

app.get('/admin/best-profession', ...handlePipeline(...admin.pipelines.findBestProfession));
app.get('/admin/best-client', ...handlePipeline(...admin.pipelines.findBestClient));

app.use(handleErrors);

export default app;
