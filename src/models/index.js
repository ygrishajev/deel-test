import Sequelize from 'sequelize';

import { init as initProfile } from './Profile';
import { init as initContract } from './Contract';
import { init as initJob } from './Job';

export const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './database.sqlite3'
});

export const Profile = initProfile(sequelize);
export const Contract = initContract(sequelize);
export const Job = initJob(sequelize);

Profile.hasMany(Contract, { as: 'Contractor', foreignKey: 'ContractorId' });
Contract.belongsTo(Profile, { as: 'Contractor' });
Profile.hasMany(Contract, { as: 'Client', foreignKey: 'ClientId' });
Contract.belongsTo(Profile, { as: 'Client' });
Contract.hasMany(Job);
Job.belongsTo(Contract);
