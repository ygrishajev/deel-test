import Sequelize from 'sequelize';

class Contract extends Sequelize.Model {}

export const init = (sequelize) =>
  Contract.init(
    {
      terms: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      status: {
        type: Sequelize.ENUM('new', 'in_progress', 'terminated')
      }
    },
    {
      sequelize,
      modelName: 'Contract'
    }
  );
