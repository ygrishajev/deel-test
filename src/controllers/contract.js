import { Op } from 'sequelize';
import assert from 'http-assert';
import { Type } from '@sinclair/typebox';

import { validate } from '../middleware';

export const findOne = async (req, res) => {
  const { Contract } = req.app.get('models');

  const contract = await Contract.findOne({
    where: {
      id: req.params.id,
      ...getClauseFor(req.currentUser)
    }
  });

  assert(contract, 404, 'contract not found');

  res.json({ data: contract });
};

const findAllSchema = {
  query: Type.Object(
    {
      status: Type.Optional(Type.String({ enum: ['terminated', 'in_progress', 'new'] }))
    },
    { additionalProperties: false }
  )
};

export const findAll = async (req, res) => {
  const { Contract } = req.app.get('models');
  const where = getClauseFor(req.currentUser);
  const { status } = req.query;

  if (status) {
    where.status = status;
  }

  const contracts = await Contract.findAll({
    where
  });

  res.json({ data: contracts });
};

function getClauseFor(user) {
  return {
    [Op.or]: [{ ContractorId: user.id }, { ClientId: user.id }]
  };
}

export const pipelines = {
  findAll: [validate(findAllSchema), findAll]
};
