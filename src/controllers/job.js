import { Op } from 'sequelize';
import assert from 'http-assert';
import { Type } from '@sinclair/typebox';

import { validate } from '../middleware';

const findAllSchema = {
  query: Type.Object(
    {
      paid: Type.Optional(Type.Boolean()),
      contractStatus: Type.Optional(Type.String({ enum: ['terminated', 'in_progress', 'new'] }))
    },
    { additionalProperties: false }
  )
};

export const findAll = async (req, res) => {
  const { Contract, Job } = req.app.get('models');
  const {
    currentUser,
    query: { contractStatus, paid }
  } = req;
  const contractsWhere = contractStatus ? { status: contractStatus } : {};
  const jobsWhere = {};

  if (typeof paid !== 'undefined') {
    jobsWhere.paid = paid ? true : null;
  }

  const jobs = await Job.findAll({
    where: jobsWhere,
    include: {
      model: Contract,
      where: {
        ...contractsWhere,
        [Op.or]: [{ ContractorId: currentUser.id }, { ClientId: currentUser.id }]
      },
      attributes: []
    }
  });

  res.json(jobs);
};

export const pay = async (req, res) => {
  const { Contract, Job, Profile } = req.app.get('models');
  const sequelize = req.app.get('sequelize');
  const { jobId } = req.params;
  const { currentUser } = req;

  assert(currentUser.isClient, 400, 'transaction invalid');

  await sequelize.transaction(async (transaction) => {
    const job = await Job.findOne({
      where: { id: jobId },
      include: {
        model: Contract,
        where: { status: { [Op.ne]: 'terminated' } },
        include: [
          { model: Profile, as: 'Client', where: { id: currentUser.id } },
          { model: Profile, as: 'Contractor' }
        ]
      },
      lock: transaction.LOCK.UPDATE
    });
    assert(job, 404, 'job not found');
    assert(!job.paid, 400, 'transaction invalid');
    assert(currentUser.balance >= job.price, 400, 'insufficient balance');

    const paymentDate = new Date();
    await Promise.all([
      job.update({ paid: 1, paymentDate }, { transaction }),
      job.Contract.Client.increment({ balance: -job.price }, { transaction }),
      job.Contract.Contractor.increment({ balance: job.price }, { transaction })
    ]);

    const { Contract: _, ...pretty } = job.toJSON();
    pretty.paid = true;
    pretty.paymentDate = paymentDate;

    return res.json({ data: pretty });
  });
};

export const pipelines = {
  findAll: [validate(findAllSchema), findAll]
};
