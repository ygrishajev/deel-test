import { col, fn, literal, Op } from 'sequelize';
import assert from 'http-assert';
import { Type } from '@sinclair/typebox';

import { validate } from '../middleware';

const dateSchema = Type.Union([
  Type.Optional(Type.String({ format: 'date' })),
  Type.Optional(Type.String({ format: 'date-time' }))
]);

const datesRagesSchemaProps = {
  start: dateSchema,
  end: dateSchema
};

const findBestProfessionSchema = {
  query: Type.Object(datesRagesSchemaProps, { additionalProperties: false })
};

export const findBestProfession = async (req, res) => {
  const { Profile, Job, Contract } = req.app.get('models');
  const group = 'Contract.Contractor.profession';

  const professions = await Job.findAll({
    where: getRangeClauseFrom(req),
    attributes: [[fn('SUM', col('Job.price')), 'totalPaid']],
    include: {
      model: Contract,
      include: {
        model: Profile,
        as: 'Contractor',
        attributes: ['profession']
      }
    },
    group,
    raw: true,
    order: literal('totalPaid DESC'),
    limit: 1
  });

  assert(professions.length, 404, 'profession not found');

  return res.json({ data: { profession: professions?.[0][group] } });
};

const findBestClientSchema = {
  query: Type.Object(
    {
      ...datesRagesSchemaProps,
      limit: Type.Optional(Type.Integer({ minimum: 0, maximum: 100 }))
    },
    { additionalProperties: false }
  )
};

export const findBestClient = async (req, res) => {
  const { Profile, Job, Contract } = req.app.get('models');

  const jobs = await Job.findAll({
    where: getRangeClauseFrom(req),
    attributes: [[fn('SUM', col('Job.price')), 'paid']],
    include: {
      model: Contract,
      include: {
        model: Profile,
        as: 'Client',
        attributes: ['id', 'firstName', 'lastName']
      }
    },
    group: 'Contract.Client.id',
    order: literal('paid DESC'),
    limit: req.query.limit || 2
  });

  const data = jobs.map((job) => {
    const client = job.Contract.Client;

    return {
      id: client.id,
      fullName: client.fullName,
      paid: job.paid
    };
  });

  return res.json({ data });
};

function getRangeClauseFrom(req) {
  const { start, end } = req.query;

  if (!start && !end) {
    return {};
  }

  assert(!start || !end || new Date(start).getTime() < new Date(end).getTime(), 'invalid dates range');

  const where = { paymentDate: {} };

  if (start) {
    Object.assign(where.paymentDate, { [Op.gte]: start });
  }

  if (end) {
    Object.assign(where.paymentDate, { [Op.lte]: end });
  }

  return where;
}

export const pipelines = {
  findBestProfession: [validate(findBestProfessionSchema), findBestProfession],
  findBestClient: [validate(findBestClientSchema), findBestClient]
};
