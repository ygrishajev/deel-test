import { fn, col } from 'sequelize';
import assert from 'http-assert';
import { Type } from '@sinclair/typebox';

import { validate } from '../middleware';

const depositSchema = {
  body: Type.Object(
    {
      data: Type.Object(
        {
          amount: Type.Number({ minimum: 0 })
        },
        { additionalProperties: false }
      )
    },
    { additionalProperties: false }
  )
};

export const deposit = async (req, res) => {
  const { Contract, Job, Profile } = req.app.get('models');
  const sequelize = req.app.get('sequelize');
  const {
    currentUser,
    body: { data }
  } = req;
  const { amount } = data;

  assert(currentUser.isClient, 400, 'transaction invalid');

  await sequelize.transaction(async (transaction) => {
    const [{ totalUnpaid }] = await Job.findAll({
      where: {
        paid: null
      },
      include: {
        model: Contract,
        where: { ClientId: currentUser.id },
        attributes: []
      },
      attributes: [[fn('sum', col('Job.price')), 'totalUnpaid']],
      raw: true,
      lock: transaction.LOCK.UPDATE
    });

    assert(amount <= totalUnpaid * 0.25, 400, 'transaction invalid');

    const { 1: count } = await Profile.increment(
      { balance: amount },
      { where: { id: currentUser.id }, returning: true },
      { transaction }
    );

    assert(count, 404, 'profile not found');

    return res.json({ data: await Profile.findByPk(currentUser.id) });
  });
};

export const pipelines = {
  deposit: [validate(depositSchema), deposit]
};
