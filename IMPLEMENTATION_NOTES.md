# Implementation notes

## Refactoring

1. To start off some dev tooling had to be set up: eslint, prettier.
2. Babel is added to get advantage of modern JS. NPM scripts where complemented with necessary commands to build dist, boot app and run seeds.   
3. Models are split into own module.

## Getting started

now to boot the project it is now required to build it first

```npm run build```

then it's possible to `npm run seed` and `npm run start` as before.

To seed in dev mode run `npm run seed:dev`

To start in dev mode run `npm run start:dev`

## General structure

New `controllers` dir contains all entity related middlewares and pipelines of such.

## Routes

**GET** `/contracts` accepts `status` query parameters which allow satisfying a goal set in the task. It is also flexible enough to go beyound the task.

**GET** `/jobs/unpaid` implemented as **GET** `/jobs` - it accepts `paid` and `contractStatus` query parameters which allow satisfying a goal set in the task. It is also flexible enough to go beyound the task.

## Tests

A test coverage for payment logic is added just as an example.
