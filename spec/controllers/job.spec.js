import { Op } from 'sequelize';
import { BadRequest, NotFound } from 'http-errors';
import { Request } from 'jest-express/lib/request';
import { Response } from 'jest-express/lib/response';

import { job } from 'src/controllers';
import { sequelize, models, transaction } from '../mocks';

const { Job, Contract, Profile } = models;

const request = new Request('/jobs/5/pay', {
  headers: {
    profile_id: 4
  }
});

beforeEach(() => {
  request.app.get.mockImplementation((key) => {
    if (key === 'models') {
      return models;
    }

    return sequelize;
  });

  request.params = { jobId: 1 };
  request.currentUser = { id: 2, isClient: true };
});

afterEach(() => {
  jest.clearAllMocks();
  jest.resetAllMocks();
});

describe('job controller', () => {
  it('rejects provided a non-client user', async () => {
    request.currentUser = { isClient: false };
    await expect(job.pay(request)).rejects.toEqual(new BadRequest('transaction invalid'));
  });

  it('looks up a job by id with non-terminated contract owned by a requester', async () => {
    await job.pay(request).catch(() => {});

    expect(Job.findOne).toHaveBeenLastCalledWith({
      where: { id: 1 },
      include: {
        model: Contract,
        where: { status: { [Op.ne]: 'terminated' } },
        include: [
          { model: Profile, as: 'Client', where: { id: 2 } },
          { model: Profile, as: 'Contractor' }
        ]
      },
      lock: transaction.LOCK.UPDATE
    });
  });

  it('rejects with NotFound error', async () => {
    await expect(job.pay(request)).rejects.toEqual(new NotFound('job not found'));
  });

  it('rejects with BadRequest provided job is already paid', async () => {
    Job.findOne.mockResolvedValue({ paid: true });
    await expect(job.pay(request)).rejects.toEqual(new BadRequest('transaction invalid'));
  });

  it('does a transfer from client to contractor and finalizes a job', async () => {
    request.currentUser = { id: 2, isClient: true, balance: 300 };
    const incrementClient = jest.fn();
    const incrementContractor = jest.fn();
    const result = {
      paid: false,
      price: 300,
      Contract: {}
    };
    Job.findOne.mockResolvedValue({
      ...result,
      update: jest.fn(),
      Contract: { Client: { increment: incrementClient }, Contractor: { increment: incrementContractor } },
      toJSON: () => result
    });
    const response = new Response();

    await expect(job.pay(request, response)).resolves.toBeUndefined();
    expect(response.json).toHaveBeenLastCalledWith({
      data: {
        paid: true,
        price: 300,
        paymentDate: expect.any(Date)
      }
    });
  });
});
