export const transaction = {
  LOCK: { UPDATE: 'UPDATE' }
};

const Job = {
  findOne: jest.fn()
};

const Contract = { name: Symbol('Contract') };
const Profile = { name: Symbol('Profile') };

export const models = {
  Job,
  Contract,
  Profile
};

export const sequelize = {
  async transaction(cb) {
    await cb(transaction);
  }
};
