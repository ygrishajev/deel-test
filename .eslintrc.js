module.exports = {
  env: {
    es2021: true,
    node: true,
    jest: true
  },
  extends: ['airbnb-base', 'prettier'],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: ['import', 'jest', 'prettier'],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js'],
        paths: ['.']
      }
    },
    'import/extensions': ['.js'],
    'import/external-module-folders': ['node_modules']
  },
  rules: {
    semi: ['error', 'always'],
    'comma-dangle': ['error', 'never'],
    'no-use-before-define': 'off',
    'arrow-parens': ['error', 'always'],
    'import/prefer-default-export': 'off',
    'no-prototype-builtins': 'off',
    'no-plusplus': 'off',
    'import/no-import-module-exports': 'off',
    'no-return-await': 'off'
  }
};
